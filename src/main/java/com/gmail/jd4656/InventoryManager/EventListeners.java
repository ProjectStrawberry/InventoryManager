package com.gmail.jd4656.InventoryManager;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;

public class EventListeners implements Listener {
    private InventoryManager inventoryManager;

    EventListeners(InventoryManager manager) {
        inventoryManager = manager;
    }

    @EventHandler
    public void InventoryCloseEvent(InventoryCloseEvent event) {
        HumanEntity player = event.getPlayer();
        Inventory inv = event.getInventory();
        if (!(player instanceof Player)) return;
        if (inventoryManager.inventoryEvents.containsKey(player.getUniqueId()) && inventoryManager.inventoryEvents.get(player.getUniqueId()).inventory.equals(inv)) {
            inventoryManager.dispose();
            inventoryManager = null;
        }
    }

    @EventHandler
    public void InventoryClickEvent(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inv = event.getClickedInventory();
        if (inv == null) return;
        if (inventoryManager.inventoryEvents.containsKey(player.getUniqueId())) {
            InventoryManager manager = inventoryManager.inventoryEvents.get(player.getUniqueId());
            if ((manager.inventory.equals(inv) || manager.inventory.equals(event.getInventory())) && manager.clickHandler != null) {
                manager.clickHandler.handle(event);
            }
        }
    }

    @EventHandler
    public void InventoryMoveItemEvent(InventoryMoveItemEvent event) {
        for (InventoryManager manager : inventoryManager.inventoryEvents.values()) {
            if (event.getSource().equals(manager.inventory)) {
                event.setCancelled(true);
                break;
            }
            if (event.getDestination().equals(manager.inventory)) {
                event.setCancelled(true);
                break;
            }
        }
    }
}
