package com.gmail.jd4656.InventoryManager;

import org.bukkit.event.inventory.InventoryClickEvent;

public abstract class InventoryClickHandler {
    public abstract void handle(InventoryClickEvent event);
}
