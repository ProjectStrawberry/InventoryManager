package com.gmail.jd4656.InventoryManager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class InventoryManager {
    public Inventory inventory;
    InventoryClickHandler clickHandler;
    Map<UUID, InventoryManager> inventoryEvents = new HashMap<>();
    JavaPlugin plugin;
    private EventListeners listeners;

    public InventoryManager(String title, int size, JavaPlugin p) {
        plugin = p;
        inventory = Bukkit.createInventory(null, size, title);
        registerEvents();
    }

    public InventoryManager(String title, InventoryType type, JavaPlugin p) {
        plugin = p;
        inventory = Bukkit.createInventory(null, type, title);
        registerEvents();
    }

    public void show(Player player) {
        inventoryEvents.remove(player.getUniqueId());
        inventoryEvents.put(player.getUniqueId(), this);
        player.openInventory(inventory);
    }

    void dispose() {
        HandlerList.unregisterAll(listeners);
    }

    private void registerEvents() {
        listeners = new EventListeners(this);
        plugin.getServer().getPluginManager().registerEvents(listeners, plugin);
    }

    public void withEventHandler(InventoryClickHandler handler) {
        clickHandler = handler;
    }

    public void withItem(int invPos, ItemStack item) {
        inventory.setItem(invPos, item);
    }
}
